.DEFAULT_GOAL:=help
SHELL:=/bin/bash


help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

build:
	docker compose build

rebuild:
	docker compose build --no-cache

start: ## Start the server on http://localhost:8000
	docker compose up -d

stop:  ## Stop all project docker containers
	docker compose down

shell: ## Run a shell, like SSH-ing into the docker machine
	docker compose run --rm --entrypoint "/bin/bash" app

restart: stop start ## Stop and start the server to restart it
