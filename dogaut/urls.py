from django.urls import re_path, include
from django.contrib import admin

urlpatterns = [
    re_path(r'^caronte/', admin.site.urls),
    re_path(r'^', include('apps.login.urls')),
    re_path(r'^dashboard/', include('apps.dashboard.urls')),
    re_path(r'^menu/', include('apps.menu.urls')),
    re_path(r'^encuesta/', include('apps.encuesta.urls')),
]
