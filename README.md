# Restaurant Dogout 2 Project

This project is a Django-based web application.


### Prerequisites

* Docker
* Docker Compose
* Make (optional, but recommended)


## Local Dev Setup

To get started just run:

``` shell
make start
```

To stop the application run:
``` shell
make stop
```

Diplay logs
``` shell
docker logs -f docker logs -f dogaut-web-1
```

You can explore more shortcut commands using `make help`.
