from django.db import models
from apps.catalogo.models import TimeStamp, ClasificadorMenu, Ingrediente
from apps.empleados.models import Empleado


class Servicio(TimeStamp):
    nombre = models.CharField(max_length=50, help_text="Nombre del platillo o bebida")
    precio = models.IntegerField(help_text='Precio de servicio')
    descripcion = models.TextField(help_text="Descripcion del platillo o bebida")
    clasificacion = models.ForeignKey(ClasificadorMenu, on_delete=models.CASCADE)

    def precioServicio(self):
        return self.precio_set.get(estatus__clave='ACT').precio

    def __str__(self):
        return "%s" % self.nombre

    class Meta:
        verbose_name_plural = 'Servicios'


class HistorialPrecio(TimeStamp):
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    precio = models.IntegerField()


class Preparacion(TimeStamp):
    servicio = models.ForeignKey(Servicio, help_text="Servicios que tenga como clasificacion alimentos y licor", on_delete=models.CASCADE)
    preparacion = models.TextField(help_text="Detalles del modo de preparacion")
    observaciones = models.TextField(help_text="Alguna observacion pertinente", null=True, blank=True)

    def __str__(self):
        return "%s %s" % (self.servicio, self.observaciones)

    class Meta:
        verbose_name_plural = "Preparacion recetas"


class TicketGeneral(TimeStamp):
    TIPO_PAGO = (
        (1, 'Efectivo'),
        (2, 'Tarjeta credito'),
        (3, 'Tarjeta debito'),
    )
    fecha = models.DateField(help_text='Fecha del consumo')
    mesa = models.IntegerField(help_text='Mesa que realizo el consumo')
    mesero = models.ForeignKey(Empleado, help_text='Mesero que atendio', on_delete=models.CASCADE)
    pago = models.IntegerField(choices=TIPO_PAGO, help_text="Tipo de pago")
    factura = models.BooleanField(help_text='Indica si se facturo el consumo')
    detalle = models.ManyToManyField('TicketDetalle', null=True, blank=True)

    def totalTicket(self):
        total = 0
        for detalle in self.ticketdetalle_set.filter():
            total += (detalle.precio * detalle.cantidad)
        return total

    def __str__(self):
        return "Mesa: %s Mesero: %s" % (self.mesa, self.mesero.alias)

    class Meta:
        verbose_name_plural = "Ticket general"


class TicketDetalle(TimeStamp):
    ticket_general = models.ForeignKey(TicketGeneral, on_delete=models.CASCADE)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    precio = models.IntegerField()
    cantidad = models.IntegerField()
    observacion = models.CharField(max_length=50, null=True, blank=True)

    def subTotal(self):
        return self.precio * self.cantidad

    def __str__(self):
        return "%s" % self.servicio

    class Meta:
        verbose_name_plural = "Ticket detalle"

