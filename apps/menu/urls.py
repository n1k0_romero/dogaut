from django.urls import re_path
from apps.menu.views import (
    TicketGeneralCreateView,
    TicketDetalleCreateView,
    TicketDetalleDeleteView,
    TicketGeneralListView,
    AjaxView,
    TicketGeneralDeleteView, 
    TicketGeneralDetailView, 
    ResumenGeneralView, 
    InicioFinConsultaView, 
    ResumenInicioFinView
)

urlpatterns = [
    re_path(r'^crear/ticket/(?P<pk>)$', TicketGeneralCreateView.as_view(), name="crear_ticket"),
    re_path(r'^borrar/ticket/(?P<pk>)$', TicketGeneralDeleteView.as_view(), name="borrar_ticket"),
    re_path(r'^detalle/ticket/(?P<pk>)$', TicketGeneralDetailView.as_view(), name="detalle_ticket"),
    re_path(r'^crear/detalle/(?P<pk>)$', TicketDetalleCreateView.as_view(), name="crear_detalle"),
    re_path(r'^borrar/detalle/(?P<pk>)$', TicketDetalleDeleteView.as_view(), name="borrar_detalle"),
    re_path(r'^listar/tickets/(?P<dia>[\w])$', TicketGeneralListView.as_view(), name="listar_ticket"),
    re_path(r'^resumen/tickets/(?P<date>\d{4}-\d{2}-\d{2})/$', ResumenGeneralView.as_view(), name="resumen_ticket"),
    re_path(r'^resumen/inicio/(?P<inicio>\d{4}-\d{2}-\d{2})/fin/(?P<fin>\d{4}-\d{2}-\d{2})$', ResumenInicioFinView.as_view(), name="resumen_inicio_fin"),
    re_path(r'^resumen/consulta/$', InicioFinConsultaView.as_view(), name="inicio_fin_consulta"),
    re_path(r'^ajax/$', AjaxView.as_view()),
]