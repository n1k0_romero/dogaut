from django import forms
from datetime import datetime
from apps.menu.models import TicketGeneral


class TicketGeneralForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
       super(TicketGeneralForm, self).__init__(*args, **kwargs)
       hoy = datetime.now()
       hoyFormateado = hoy.strftime("%Y-%m-%d")
       self.fields['fecha'].initial = hoyFormateado
       self.fields['pago'].initial = 1

    class Meta:
        model = TicketGeneral
        fields = ['fecha', 'mesero', 'pago', 'factura']
        widgets = {
                'fecha': forms.DateTimeInput(attrs={'class': 'form-control datepicker'}),
                'mesero': forms.Select(attrs={'class': 'form-control'}),
                'pago': forms.Select(attrs={'class': 'form-control'}),
                'factura': forms.CheckboxInput(attrs={'class': 'form-control'}),
        }


class InicioFinConsultaForm(forms.Form):
    inicio = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control datepicker'}))
    fin = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control datepicker'}))

