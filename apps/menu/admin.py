from django.contrib import admin
from reversion.admin import VersionAdmin
from apps.menu.models import Servicio, Preparacion,TicketGeneral


@admin.register(Servicio)
class ServicioAdmin(VersionAdmin):
    list_display = ('id', 'nombre', 'clasificacion')
    list_display_links = ('id', 'nombre', 'clasificacion')


@admin.register(Preparacion)
class PreparacionAdmin(VersionAdmin):
    list_display = ('id', 'servicio')
    list_display_links = ('id', 'servicio')


@admin.register(TicketGeneral)
class TicketGeneralAdmin(VersionAdmin):
    list_display = ('id', 'fecha', 'mesa')
    list_display_links = ('id', 'fecha', 'mesa')