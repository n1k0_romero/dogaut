from django.views.generic import CreateView, DeleteView, ListView, TemplateView, DetailView, FormView
from django import forms
from django.db import transaction
from braces.views import GroupRequiredMixin
from django.db.models import Sum
from django.core import serializers
from django.http import HttpResponse
from django.contrib import messages
from datetime import datetime, timedelta
from django.urls import reverse
from apps.menu.models import TicketGeneral, TicketDetalle, Servicio
from apps.menu.forms import TicketGeneralForm, InicioFinConsultaForm


class TicketGeneralCreateView(GroupRequiredMixin, FormView):
    form_class = TicketGeneralForm
    template_name = 'plantillas/crear.html'
    group_required = [u'barman']
    raise_exception = True
    ticketID = 0

    def get_context_data(self, **kwargs):
        context = super(TicketGeneralCreateView, self).get_context_data(**kwargs)
        data = {
            'title': 'Crear Ticket Mesa ',
            'mesa': self.kwargs['pk'],
            'medidaInput' : 4,
        }
        context.update(data)
        return context

    @transaction.atomic()
    def form_valid(self, form):
        form.instance.usuario_crea = self.request.user
        form.instance.mesa = self.kwargs['pk']
        f = form.save()
        self.ticketID = f.id
        messages.success(self.request, "Creado %s con exito" % (f))
        return super(TicketGeneralCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('crear_detalle', kwargs={'pk': self.ticketID })


class TicketGeneralDeleteView(GroupRequiredMixin, DeleteView):
    model = TicketGeneral
    template_name = 'plantillas/borrar.html'
    success_message = "La comanda se elimino con exito"
    group_required = [u'barman']
    raise_exception = True

    def delete(self, request, *args, **kwargs):
        messages.warning(self.request, self.success_message)
        return super(TicketGeneralDeleteView, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('listar_ticket', kwargs={'dia': 'hoy'})


class TicketGeneralListView(GroupRequiredMixin, ListView):
    model = TicketGeneral
    template_name = 'plantillas/lista.html'
    ordering = 'id'
    group_required = [u'barman']
    raise_exception = True

    def get_context_data(self, **kwargs):
        hoy = datetime.now()
        context = super(TicketGeneralListView, self).get_context_data(**kwargs)
        if self.kwargs.get('dia') == 'hoy':
            dia = hoy.strftime("%Y-%m-%d")
        if self.kwargs.get('dia') == 'ayer':
            ayer = hoy - timedelta(days=1)
            dia = ayer.strftime("%Y-%m-%d")
        data = {
            'title': 'Resumen %s' % (dia),
            'empty_message': 'Parece que aun no se registra ninguna venta',
            'borrar_url': 'borrar_ticket',
            'detalle_url': 'detalle_ticket',
            'editar_url': 'crear_detalle',
            'resumen_url': 'resumen_ticket',
            'dia': dia,
        }
        context.update(data)
        return context

    def get_queryset(self):
        hoy = datetime.now()
        ayer = hoy - timedelta(days=1)
        if self.kwargs.get('dia')== 'hoy':
            queryset = self.model.objects.filter(fecha__year=hoy.year, fecha__month=hoy.month, fecha__day=hoy.day)
        if self.kwargs.get('dia')== 'ayer':
            queryset = self.model.objects.filter(fecha__year=ayer.year, fecha__month=ayer.month, fecha__day=ayer.day)
        return queryset


class TicketGeneralDetailView(DetailView):
    model = TicketGeneral
    template_name = 'menu/detalle_ticket.html'


class ResumenGeneralView(TemplateView):
    template_name = 'menu/resumen.html'

    def get_context_data(self, **kwargs):
        context = super(ResumenGeneralView,self).get_context_data(**kwargs)
        dia = self.kwargs['date']
        serviciosTotales = TicketDetalle.objects.values('servicio__nombre').annotate(cantidad=Sum('cantidad')).\
                filter(ticket_general__fecha=dia).order_by('servicio__id')
        # total en dinero
        detalles = TicketDetalle.objects.filter(ticket_general__fecha=dia)
        totalMonetario = 0
        for detalle in detalles:
            totalMonetario += detalle.precio * detalle.cantidad
        data = {
            'title' : 'Resumen %s' %(dia),
            'totalMonetario' : totalMonetario,
            'serviciosTotales': serviciosTotales,
        }
        context.update(data)
        return context


class TicketDetalleCreateView(GroupRequiredMixin, CreateView):
    model = TicketDetalle
    fields = ['servicio', 'precio', 'cantidad', 'observacion']
    template_name = 'menu/crear_detalle.html'
    group_required = [u'barman']
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(TicketDetalleCreateView, self).get_context_data(**kwargs)
        ticketGeneral = TicketGeneral.objects.get(id=self.kwargs['pk'])
        detalles = TicketDetalle.objects.filter(ticket_general=ticketGeneral)
        total = 0
        for d in detalles:
            total += d.precio * d.cantidad
        data = {
            'titleDetalle': 'Comanda',
            'ticketGeneral': ticketGeneral,
            'title' : 'Agregar alimento bebida postre',
            'medidaInput' : 6,
            'total' : total,
            'borrar_url' : 'borrar_detalle',
            'ajax_url' : 'ajax',
        }
        context.update(data)
        return context

    @transaction.atomic()
    def get_form(self, form_class=None):
        form = super(TicketDetalleCreateView, self).get_form()
        form.fields['servicio'].widget = forms.Select(attrs={'class':'form-control'})
        form.fields['servicio'].queryset = Servicio.objects.all()
        form.fields['precio'].widget = forms.TextInput(attrs={'class':'form-control'})
        form.fields['observacion'].widget = forms.Textarea(attrs={'class':'form-control', 'rows' : 3})
        form.fields['cantidad'].widget = forms.TextInput(attrs={'class':'form-control'})
        form.initial = {'cantidad' : 1}
        return form

    def form_valid(self, form):
        form.instance.usuario_crea = self.request.user
        form.instance.ticket_general = TicketGeneral.objects.get(id=self.kwargs['pk'])
        f = form.save()
        messages.success(self.request, "Creado %s con exito" % (f))
        return super(TicketDetalleCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('crear_detalle', kwargs={'pk': self.kwargs['pk']})


class TicketDetalleDeleteView(GroupRequiredMixin, DeleteView):
    model = TicketDetalle
    template_name = 'plantillas/borrar.html'
    success_message = "El servicio se elimino con exito"
    group_required = [u'barman']
    raise_exception = True

    def delete(self, request, *args, **kwargs):
        messages.warning(self.request, self.success_message)
        return super(TicketDetalleDeleteView, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('crear_detalle', kwargs={'pk': self.object.ticket_general.id})


class AjaxView(TemplateView):

    def get(self,request, *args, **kwargs):
        envioAjax = request.GET['envio']
        servicios = Servicio.objects.filter(nombre__contains=envioAjax.upper())
        data = serializers.serialize('json', servicios, fields=('id', 'nombre', 'precio'))
        return HttpResponse(data)


class InicioFinConsultaView(FormView):
    form_class = InicioFinConsultaForm
    template_name = 'plantillas/crear.html'

    def get_context_data(self, **kwargs):
        contex = super(InicioFinConsultaView,self).get_context_data(**kwargs)
        data = {
            'title' : 'Cosultar ventas',
        }
        contex.update(data)
        return contex

    def form_valid(self, form):
        messages.success(self.request, "Consulta exitosa")
        return super(InicioFinConsultaView,self).form_valid(form)

    def get_success_url(self):
        return reverse('resumen_inicio_fin', kwargs={'inicio': self.request.POST['inicio'], 'fin': self.request.POST['fin']})


class ResumenInicioFinView(TemplateView):
    template_name = 'menu/resumen.html'

    def get_context_data(self, **kwargs):
        context = super(ResumenInicioFinView,self).get_context_data(**kwargs)
        inicio = self.kwargs['inicio']
        fin = self.kwargs['fin']
        serviciosTotales = TicketDetalle.objects.values('servicio__nombre').annotate(cantidad=Sum('cantidad')).\
                filter(ticket_general__fecha__range=[inicio,fin]).order_by('servicio__id')
        # total en dinero
        detalles = TicketDetalle.objects.filter(ticket_general__fecha__range=[inicio,fin])
        totalMonetario = 0
        for detalle in detalles:
            totalMonetario += detalle.precio * detalle.cantidad
        data = {
            'title' : 'Resumen del %s al %s' %(inicio, fin),
            'totalMonetario' : totalMonetario,
            'serviciosTotales': serviciosTotales,
        }
        context.update(data)
        return context