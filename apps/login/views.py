from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.views.generic import TemplateView, View
from django.urls import reverse_lazy 


class LoginView(TemplateView):
    template_name = 'login/login.html'


class AccesoView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse_lazy('inicio'))

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        usuario = authenticate(username=username, password=password)

        if usuario is not None:
            if usuario.is_active:
                login(request, usuario)
                if username is not 'admin':
                    return HttpResponseRedirect(reverse_lazy('dashboard'))
            else:
                messages.error(self.request, 'El usuario <b>%s</b> no esta activo.' % username, extra_tags='safe')
                return HttpResponseRedirect(reverse_lazy('inicio'))
        else:
            messages.error(self.request, 'No se encontro el usuario <b>%s</b>.' % username, extra_tags='safe')
            return HttpResponseRedirect(reverse_lazy('inicio'))


