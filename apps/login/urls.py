from django.urls import re_path
from.views import LoginView, AccesoView

urlpatterns = [
    re_path(r'^accesar$', AccesoView.as_view(), name="accesar"),
    re_path(r'^cerrar$', AccesoView.as_view(), name="cerrar-sesion"),
    re_path(r'^$', LoginView.as_view(), name="inicio"),
]
