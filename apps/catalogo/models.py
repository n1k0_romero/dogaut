from django.db import models
from django.contrib.auth.models import User


class Estatus(models.Model):
    nombre = models.CharField(max_length=30)
    clave = models.CharField(max_length=4, unique=True)

    def __str__(self):
        return "%s - %s , %s" % (self.id, self.nombre, self.clave)

    class Meta:
        verbose_name_plural = 'Estatus'


class TimeStamp(models.Model):
    usuario_crea = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    creado = models.DateTimeField(auto_now_add=True, null=True)
    modificado = models.DateTimeField(auto_now=True, null=True)
    estatus = models.ForeignKey(Estatus, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Funcion(models.Model):
    nombre = models.CharField(max_length=40)
    clave = models.CharField(max_length=5)

    def __str__(self):
        return "%s - %s" % (self.clave, self.nombre)

    class Meta:
        verbose_name_plural = 'Funciones'


class ClasificadorMenu(models.Model):
    nombre = models.CharField(max_length=20)
    clave = models.CharField(max_length=4, unique=True)

    def __str__(self):
        return "%s - %s" % (self.clave, self.nombre)

    class Meta:
        verbose_name_plural = 'Clasificador Menu'


class Ingrediente(models.Model):
    nombre = models.CharField(max_length=50, help_text="Nombre del ingrediente")
    descripcion = models.CharField(max_length=50, help_text="Breve descripcion del ingrediente")

    def __str__(self):
        return "%s - %s" % (self.nombre, self.descripcion)

    class Meta:
        verbose_name_plural = "Ingredientes"