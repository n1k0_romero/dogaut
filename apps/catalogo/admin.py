from django.contrib import admin
from reversion.admin import VersionAdmin
from apps.catalogo.models import Estatus, Funcion, ClasificadorMenu


@admin.register(Estatus)
class EstatusAdmin(VersionAdmin):
    list_display = ('id', 'nombre', 'clave')
    list_display_links = ('id', 'nombre', 'clave')


@admin.register(Funcion)
class FuncionAdmin(VersionAdmin):
    list_display = ('id', 'nombre', 'clave')
    list_display_links = ('id', 'nombre', 'clave')


@admin.register(ClasificadorMenu)
class ClasificadorMenuAdmin(VersionAdmin):
    list_display = ('id', 'nombre', 'clave')
    list_display_links = ('id', 'nombre', 'clave')