from django.apps import AppConfig


class CatalogoConfig(AppConfig):
    name = 'apps.catalogo'
