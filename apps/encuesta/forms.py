from django import forms
from apps.encuesta.models import Satisfaccion


class SatisfaccionForm(forms.ModelForm):

    class Meta:
        model = Satisfaccion
        fields = ['plaza', 'fastfood',]
        widgets = {
                'plaza': forms.Select(attrs={'class': 'form-control'}),
                'fastfood': forms.Select(attrs={'class': 'form-control'}),
        }