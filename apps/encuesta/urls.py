from django.urls import re_path  
from apps.encuesta.views import SatisfaccionCreateView


urlpatterns = [
    re_path(r'^satisfaccion$', SatisfaccionCreateView.as_view(), name="encuesta-satisfaccion"),
]
