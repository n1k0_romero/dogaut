from django.contrib import admin
from reversion.admin import VersionAdmin
from apps.encuesta.models import Satisfaccion


@admin.register(Satisfaccion)
class SatisfaccionAdmin(VersionAdmin):
    list_display = ('id', )
    list_display_links = ('id',)
