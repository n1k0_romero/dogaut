from django.db import models


class Satisfaccion(models.Model):
    OPCIONES = (
        (0, 'Bueno'),
        (1, 'Regular'),
        (2, 'Malo'),
    )
    plaza = models.IntegerField(choices=OPCIONES, help_text='¿Como calificaria el servicio de la plaza?', default=0)
    fastfood = models.IntegerField(choices=OPCIONES, help_text="¿Como calificaria el servicio de fastfood?", default=0)

    def __str__(self):
        return "%s" % self.id

    class Meta:
        verbose_name_plural = 'Encuesta satisfaccion'

