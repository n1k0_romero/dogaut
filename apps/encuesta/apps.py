from django.apps import AppConfig


class EncuestaConfig(AppConfig):
    name = 'apps.encuesta'
