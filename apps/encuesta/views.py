from django.views.generic import FormView
from django.db import transaction
from django.contrib import messages
from django.urls import reverse_lazy
from apps.encuesta.forms import SatisfaccionForm


class SatisfaccionCreateView(FormView):
    form_class = SatisfaccionForm
    template_name = 'plantillas/crear.html'
    success_url = 'encuesta-satisfaccion'

    def get_context_data(self, **kwargs):
        context = super(SatisfaccionCreateView, self).get_context_data(**kwargs)
        data = {
            'title': 'Encuesta de satisfaccion',
            'medidaInput' : 2,
        }
        context.update(data)
        return context

    @transaction.atomic()
    def form_valid(self, form):
        f = form.save()
        messages.success(self.request, " Tenemos tu opinion mil gracias :D")
        return super(SatisfaccionCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('encuesta-satisfaccion')
