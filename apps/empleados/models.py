from django.db import models
from apps.catalogo.models import TimeStamp, Funcion


class Empleado(TimeStamp):
    nombres = models.CharField(max_length=50, help_text="Nombre del empleado")
    apellido_paterno = models.CharField(max_length=30, help_text="Apellido paterno del empleado")
    apellido_materno = models.CharField(max_length=30, help_text="Apellido materno del empleado")
    alias = models.CharField(max_length=15, unique=True, help_text="Apodo del empleado")
    funcion = models.ForeignKey(Funcion, help_text="que funcion desempeña el empleado", on_delete=models.CASCADE)
    fecha_nacimiento = models.DateField(auto_now=False, auto_now_add=False, null=True, help_text="Fecha de nacimiento del empleado")
    fecha_ingreso = models.DateField(auto_now=False, auto_now_add=False, null=True, help_text="Fecha de contratacion del empleado")
    curp = models.CharField(max_length=18, null=True, blank=True, help_text="CURP del empleado")
    rfc = models.CharField(max_length=13, null=True, blank=True, help_text="RFC del empleado")

    def __str__(self):
        return "%s" % self.alias

    class Meta:
        verbose_name_plural = 'Empleados'