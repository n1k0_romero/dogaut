from django.contrib import admin
from reversion.admin import VersionAdmin
from apps.empleados.models import Empleado


@admin.register(Empleado)
class EmpleadoAdmin(VersionAdmin):
    list_display = ('id', 'nombres', 'apellido_paterno', 'apellido_materno', 'alias')
    list_display_linls = ('id', 'nombres', 'apellido_paterno', 'apellido_materno', 'alias')
