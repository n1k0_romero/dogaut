from django.views.generic import TemplateView
from braces.views import LoginRequiredMixin, GroupRequiredMixin


class DashboardView(LoginRequiredMixin, GroupRequiredMixin, TemplateView):
    template_name = 'dashboard/dashboard.html'
    login_url = "inicio"
    group_required = [u'barman']
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        data = {
            'title' : 'Registrar venta',
            'url_crear' : 'crear_ticket',
        }
        context.update(data)
        return context

